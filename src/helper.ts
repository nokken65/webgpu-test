export const checkWebGPU = () => {
  let result = "WebGPU is available!";
  if (!navigator.gpu) {
    result = "WebGPU isn't supported!";
  }
  return result;
};
